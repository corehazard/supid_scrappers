module.exports = {
    click: async function (page, selector) {
        try {
            await page.waitForSelector(selector);
            await page.click(selector);
        } catch (error) {
            throw new Error(`Could not click on selector: ${selector}`);
        }
    },

    evClick: async function (page, selector) {
        try {
            await page.evaluate((selector) => document.querySelector(selector).click(selector), selector);
        } catch (error) {
            throw new Error(`Could not evaluate click on selector: ${selector}`);
        }
    },

    getFrame: async function (page, name) {
        let frame = page.mainFrame();

        return frame.childFrames().find(frame => frame.name() === name);
    },

    typeText: async function (page, text, selector) {
        try {
            await page.waitForSelector(selector);
            await page.type(selector, text);
        } catch (error) {
            throw new Error(`Could not type text into selector: ${selector}. Error: ${error}`);
        }
    },

    loadUrl: async function (page, url) {
        await page.goto(url, { waitUntil: "networkidle0" });
    },

    hover: async function (page, selector) {
        try {
            await page.waitForSelector(selector);
            await page.hover(selector);
        } catch (error) {
            throw new Error(`Cannot hover selector: ${selector}`);
        }
    },

    getText: async function (page, selector) {
        try {
            await page.waitForSelector(selector);
            return page.$eval(selector, e => e.textContent);
        } catch (error) {
            throw new Error(`Cannot get text from selector: ${selector}`);
        }
    },

    getAllElements: async function (page, selector) {
        try {
            await page.waitForSelector(selector);
            return await page.evaluate((selector) => Array.from(document.querySelectorAll(selector), e => e.innerHTML), selector);
        } catch (error) {
            throw new Error(`Cannot get element from selector: ${selector}. Error: ${error}`);
        }
    },

    getAllText: async function (page, selector) {
        try {
            await page.waitForSelector(selector);
            return await page.evaluate((selector) => Array.from(document.querySelectorAll(selector), e => e.textContent), selector);
        } catch (error) {
            throw new Error(`Cannot get text from selector: ${selector}. Error: ${error}`);
        }
    },

    getLink: async function (page, selector) {
        try {
            await page.waitForSelector(selector);
            return page.$eval(selector, e => e.href);
        } catch (error) {
            throw new Error(`Cannot get text from selector: ${selector}`);
        }
    },

    getSrc: async function (page, selector) {
        try {
            await page.waitForSelector(selector);
            return page.$eval(selector, e => e.src);
        } catch (error) {
            throw new Error(`Cannot get text from selector: ${selector}`);
        }
    },

    getAllLink: async function (page, selector) {
        try {
            await page.waitForSelector(selector);
            return await page.evaluate((selector) => Array.from(document.querySelectorAll(selector), e => e.href), selector);
        } catch (error) {
            throw new Error(`Cannot get link from selector: ${selector}. Error: ${error}`);
        }
    },

    getCount: async function (page, selector) {
        try {
            await page.waitForSelector(selector);
            return page.$$eval(selector, items => items.length);
        } catch (error) {
            throw new Error(`Cannot get count of selector: ${selector}`);
        }
    },

    waitForText: async function (page, selector, text) {
        try {
            await page.waitForSelector(selector);
            await page.waitForFunction(
                (selector, text) =>
                    document.querySelector(selector).innerText.includes(text),
                {},
                selector,
                text
            );
        } catch (error) {
            throw new Error(`Text: ${text} not found for selector ${selector}`);
        }
    },

    pressKey: async function (page, key) {
        try {
            await page.keyboard.press(key);
        } catch (error) {
            throw new Error(`Could not press key: ${key} on the keyboard`);
        }
    },

    exists: async function (page, selector) {
        return (await page.$(selector) !== null);
    },

    shouldExist: async function (page, selector) {
        try {
            await page.waitForSelector(selector, { visible: true });
            return true;
        } catch (error) {
            return false;
        }
    },

    shouldNotExist: async function (page, selector) {
        try {
            await page.waitFor(() => !document.querySelector(selector));
        } catch (error) {
            throw new Error(`Selector: ${selector} is visible, but should not`);
        }
    },

    sleep: async function (page, timeInMs) {
        await page.waitFor(timeInMs);
    },

    autoScroll: async function (page) {
        // NOT WORKING
        await page.evaluate(async () => {
            await new Promise((resolve) => {
                var totalHeight = 0;
                var distance = 100;
                var timer = setInterval(() => {
                    var scrollHeight = document.body.scrollHeight;
                    window.scrollBy(0, distance);
                    totalHeight += distance;

                    if (totalHeight >= scrollHeight) {
                        clearInterval(timer);
                        resolve();
                    }
                }, 400);
            });
        });
    },
};