module.exports = {
    supermercado: {
        superKoch: "https://www.superkoch.com.br"
    },
    isHeadless: true,
    slowMo: 0,
    isDevtools: false,
    launchTimeout: 60000,
    waitingTimeout: 120000,
    viewportWidth: 1366,
    viewportHeight: 950,
};