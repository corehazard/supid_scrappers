const config = require("../lib/config"),
    $ = require("cheerio"),
    fs = require("fs"),
    {
        click,
        getAllText,
        getAllElements,
        getText,
        loadUrl,
        exists,
        hover
    } = require("../lib/helpers"),
    puppeteer = require("puppeteer");

// eslint-disable-next-line no-undef
const filePath = `${__dirname}/../files/supermercados/superKoch`;

if (!fs.existsSync(filePath)) {
    fs.mkdirSync(filePath, { recursive: true });
}

let browser,
    page,
    products = require("../files/supermercados/superKoch/products.json");

async function init() {
    browser = await puppeteer.launch({
        headless: config.isHeadless,
        slowMo: config.slowMo,
        devtools: config.isDevtools,
        timeout: config.launchTimeout,
    });
    page = await browser.newPage();
    page.setDefaultTimeout(config.waitingTimeout);

    await page.setViewport({
        width: config.viewportWidth,
        height: config.viewportHeight,
    });

    await loadUrl(page, `${config.supermercado.superKoch}/`);

    await hover(page, "a.menu-btn-item");
    let departamentos = [...new Set(await getAllText(page, "div.list-categ-item > div.item span.text"))].map(departamento => departamento.toLowerCase().replace(/(\s&\s|\s+)/g, "-").replace(/&/g, "").normalize("NFD").replace(/[\u0300-\u036f]/g, "")).filter(departamento => !departamento.match(/promo..es/));

    for (let departamento of departamentos) {
        console.log(`Departamento: ${departamento}\n`);
        await loadUrl(page, `${config.supermercado.superKoch}/categoria/${departamento}`);

        if (await exists(page, "a[ng-if='showCategorias == false']"))
            await click(page, "a[ng-if='showCategorias == false']");

        let categorias = [...new Set(await getAllText(page, "section.box-filtro div.item:not(.search-product) a"))].map(categoria => categoria.toLowerCase().replace(/(\s&\s|\s+)/g, "-").replace(/&/g, "").normalize("NFD").replace(/[\u0300-\u036f]/g, "")).filter(categoria => !categoria.match(/(-mais|-ocultar)/));

        for (let categoria of categorias) {
            console.log(`Categoria: ${categoria}\n`);

            await loadUrl(page, `${config.supermercado.superKoch}/categoria/${departamento}/${categoria}`);

            if (await exists(page, "a[ng-if='showCategorias == false']"))
                await click(page, "a[ng-if='showCategorias == false']");

            let subCategorias = await exists(page, "section.box-filtro div.item:not(.search-product) a")
                ? [...new Set(await getAllText(page, "section.box-filtro div.item:not(.search-product) a"))].map(categoria => categoria.toLowerCase().replace(/(\s&\s|\s+)/g, "-").replace(/&/g, "").replace(/\./g, "").normalize("NFD").replace(/[\u0300-\u036f]/g, "")).filter(categoria => !categoria.match(/(-mais|-ocultar)/))
                : [categoria];

            for (let subCategoria of subCategorias) {
                if (products.find(product => product.subCategoria == subCategoria))
                    continue;

                await loadUrl(page, `${config.supermercado.superKoch}/categoria/${departamento}/${categoria}/${subCategoria}`);

                console.log(`${config.supermercado.superKoch}/categoria/${departamento}/${categoria}/${subCategoria}`);

                let produtos = await getAllElements(page, "div.lista-produtos div.produto");
                let paginas = parseInt((await getText(page, "ul.pagination li:last-of-type")).trim());
                let pagina = 1;

                while (pagina < paginas) {
                    pagina++;
                    await loadUrl(page, `${config.supermercado.superKoch}/categoria/${departamento}/${categoria}/${subCategoria}?pg=${pagina}`);

                    produtos = [...produtos, ...await getAllElements(page, "div.lista-produtos div.produto")];
                }

                for (let produto of produtos) {
                    products.push({
                        departamento,
                        categoria,
                        subCategoria,
                        titulo: $(produto).find("div.title a").text().trim(),
                        marca: $(produto).find("div.fabricante").text().trim(),
                        imagem: $(produto).find("div.box-img img.produto-img").attr("data-original"),
                        precoPor: typeof $(produto).find("div.preco-por") !== "undefined" ? $(produto).find("div.preco-por").text().trim().replace("Por: R$ ", "").replace(/\s-\d+%/, "") : "",
                        precoDe: typeof $(produto).find("div.preco-de") !== "undefined" ? $(produto).find("div.preco-de").text().trim().replace("De: R$ ", "") : "",
                    });
                }
            }

            fs.writeFileSync(`${filePath}/products.json`, JSON.stringify(products, null, 2));
        }

        fs.writeFileSync(`${filePath}/products.json`, JSON.stringify(products, null, 2));
    }

    fs.writeFileSync(`${filePath}/products.json`, JSON.stringify(products, null, 2));
    await browser.close();
}

init();
